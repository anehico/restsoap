package restsoap

import grails.gorm.transactions.Transactional
import static java.lang.System.out;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

@Transactional
class ValidacionesService {

    def validarCampos(params) {
        println(LocalDate.now())
        if(params.nombres==null || params.nombres.isEmpty()){
            throw new Exception("El campo nombres está vacío")
        }
         if(params.apellidos==null || params.apellidos.isEmpty()){
            throw new Exception("El campo apellidos está vacío")
        }
         if(params.tipoDocumento==null || params.tipoDocumento.isEmpty()){
            throw new Exception("El campo tipoDocumento está vacío")
        }
         if(params.numeroDocumento==null || params.numeroDocumento.isEmpty()){
            throw new Exception("El campo numeroDocumento está vacío")
        }
         if(params.cargo==null || params.cargo.isEmpty()){
            throw new Exception("El campo cargo está vacío")
        }
        if(params.salario==null || params.salario.isEmpty()){
            throw new Exception("El campo salario está vacío")
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        long yearsBetween = ChronoUnit.YEARS.between(LocalDate.parse(params.fechaNacimiento,formatter), LocalDate.now());
        
        if(yearsBetween < 18){
            throw new Exception("El empleado es menor de edad")
        }
    }
}
