package restsoap
import grails.rest.*
import java.time.LocalDate

@Resource(uri='/empleado')
class Empleado {
    String nombres
    String apellidos
    String tipoDocumento
    String numeroDocumento
    LocalDate fechaNacimiento
    LocalDate fechaVinculacion
    String cargo
    Double salario

    static constraints = {
        nombres nullable: false
        apellidos nullable: false
        tipoDocumento nullable: false  
        numeroDocumento nullable: false
        fechaNacimiento nullable: false
        fechaVinculacion nullable: false
        cargo nullable: false
        salario nullable: false
    }
}
