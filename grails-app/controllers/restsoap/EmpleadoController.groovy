package restsoap

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY
import static java.lang.System.out;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

import grails.converters.*
import groovy.json.JsonSlurper 

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional

@ReadOnly
class EmpleadoController {

    EmpleadoService empleadoService
    ValidacionesService validaciones

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond empleadoService.list(params), model:[empleadoCount: empleadoService.count()]
    }

    def show(Long id) {
        respond empleadoService.get(id)
    }

    @Transactional
    def save(Empleado empleado) {
        if (empleado == null) {
            render status: NOT_FOUND
            return
        }
        if (empleado.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond empleado.errors
            return
        }

        try {
            empleadoService.save(empleado)
        } catch (ValidationException e) {
            respond empleado.errors
            return
        }

        respond empleado, [status: CREATED, view:"show"]
    }

    @Transactional
    def update(Empleado empleado) {
        if (empleado == null) {
            render status: NOT_FOUND
            return
        }
        if (empleado.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond empleado.errors
            return
        }

        try {
            empleadoService.save(empleado)
        } catch (ValidationException e) {
            respond empleado.errors
            return
        }

        respond empleado, [status: OK, view:"show"]
    }

    @Transactional
    def delete(Long id) {
        if (id == null || empleadoService.delete(id) == null) {
            render status: NOT_FOUND
            return
        }

        render status: NO_CONTENT
    }
    
    def saveEmpleado() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        validaciones=new ValidacionesService()
        try{
            validaciones.validarCampos(params)
            Empleado empleado= new Empleado()
            empleado.nombres=params.nombres
            empleado.apellidos=params.apellidos
            empleado.tipoDocumento=params.tipoDocumento
            empleado.numeroDocumento=params.numeroDocumento
            empleado.fechaNacimiento=LocalDate.parse(params.fechaNacimiento,formatter)
            empleado.fechaVinculacion= LocalDate.parse(params.fechaVinculacion,formatter)
            empleado.cargo=params.cargo
            empleado.salario=Double.parseDouble(params.salario);  

            System.out.println(params);
            render status: OK
            
        }catch(DateTimeParseException e){
            def response =["error":1, "message": "Las fechas deben ser en formato dd-MM-yyyy"]
            println(e)
            render response as JSON
        }catch(Exception e){
            def response =["error":1, "message": e.message]
            println(e)
            render response as JSON   
        }
    }
}
